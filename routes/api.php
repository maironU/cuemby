<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlayerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1'], function() {
    Route::get('/players', [PlayerController::class, 'index'])->middleware('token');
    Route::get('/players/{id}', [PlayerController::class, 'show'])->middleware('token');
    Route::post('/team', [PlayerController::class, 'playersPerTeam'])->middleware('token');
    Route::get('/fut21/update/{num_pages?}', [PlayerController::class, 'updateFut21']);
});

