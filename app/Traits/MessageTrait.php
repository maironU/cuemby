<?php
    namespace App\Traits;

    trait MessageTrait {

        public function successResponse($page, $total_pages, $total_nums, $nums, $data) {
            return response()->json([
                "page" => intval($page),
                "totalPages" => $total_pages,
                "items" => $nums,
                "totalItems" => $total_nums,
                "players" => $data,
            ]);
        }

    }
?>
