<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Traits\MessageTrait;

class PlayerController extends Controller
{
    use MessageTrait;

    private $player;

    public function __construct(Player $player){
        $this->player = $player;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = strtolower($request->search);
        $order = $request->order ? $request->order : "asc";
        $page = $request->page > 0 ? $request->page : 1;
        $nums = 10;

        $players = $this->player->searchName($search);

        $total_pages = ceil($players->count()/$nums);
        $total_nums = $players->count();

        $players = $players->pagination($nums, $page)
        ->orderName($order)
        ->get();

        return $this->successResponse($page, $total_pages, $total_nums, $nums, $players);
    }

    public function playersPerTeam(Request $request){
        $name = strtolower($request->name);
        $page = $request->page > 0 ? $request->page : 1;
        $nums = 10;

        $players = $this->player->searchClub($name);
        $total_pages = ceil($players->count()/$nums);
        $total_nums = $players->count();

        $players = $players->pagination($nums, $page)
        ->get();
        return $this->successResponse($page, $total_pages, $total_nums, $nums, $players);
    }

    public function show($id){
        try{
            $player = $this->player->findOrFail($id);
            return response()->json(["success" => true, "data" => $player]);
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json(["success" => false, "message" => "Jugador no encontrado"]);
        }
    }

    public function updateFut21($num_pages = 10){
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('players')->truncate();

        for ($i=1; $i <= $num_pages; $i++) {
            $response=Http::get("https://www.easports.com/fifa/ultimate-team/api/fut/item?page={$i}");
            if($response->successful()){
                $data = $response->json();

                foreach($data["items"] as $player){
                    $this->player::create([
                        "name" => $player['firstName']." ".$player["lastName"],
                        "position" => $player['position'],
                        "nation" => $player['nation']['name'],
                        "club" => $player['club']['name']
                    ]);
                }
            }
        }

        return "Data actualizada correctamente";
    }
}
