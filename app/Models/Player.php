<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $fillable = ["name", "position", "nation", "club"];
    protected $table = "players";
    protected $primaryKey = "id";

    public function scopeSearchClub($query, $name){
        return $query->whereRaw('LOWER(club) LIKE (?) ',["%{$name}%"]);
    }

    public function scopeSearchName($query, $name){
        return $query->whereRaw('LOWER(name) LIKE (?) ',["%{$name}%"]);
    }

    public function scopeOrderName($query, $order){
        return $query->orderBy("name", $order);
    }

    public function scopePagination($query, $nums, $page){
        return $query->take($nums)
        ->skip(($page*$nums) - $nums);
    }
}
