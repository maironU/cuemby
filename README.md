Pasos para instalar proyecto

1) Clonar el repositorio correspondiente git clone https://maironU@bitbucket.org/maironU/cuemby.git luego acceder a la carpeta cd cuemby/

2)Instalar las dependencias con composer con el comando composer install

3) Instalar las dependencias correspondientes con el comando npm install

4) crear un archivo .env en la raiz del proyecto, copiar lo que tiene el archivo .env.example y pegarla en el nuevo archivo .env

5) Configurar base de datos en el archivo .env solo cambiar el DB_DATABASE y la DB_PASSWORD(solo si es necesario)

6) Generar la key correspondiente con el comando php artisan key:generate

7) Borrar la cache php artisan config:cache

8) Correr las migraciones con el comando php artisan migrate


9) Ejecutar el servidor con el comando php artisan ser

10) Hay una ruta que carga la data del api de FIFA que me facilitaron acceder a http://127.0.0.1:8000/api/v1/fut21/update/{num_pages?}, reemplazar el host si es necesario, el parametro num_pages es opcional, por defecto se guardan 10 paginas del api, pero si quiere guardar mas o menos simplemente mandele ese parametro, esperar unos momentos mientras carga la data, ser paciente

11) Compilar la Aplicación de react con npm run dev

Listo eso es todo! solo tendrias que abrir la app con la url por defecto http://127.0.0.1:8000

Dato importante!
Las rutas están protegidas con x-api-key por defecto se carga desde las variables de entorno como TOKEN = al token que se haya puesto en esa variable

Así que cada vez que se envíe una petición exceptuando la de actualizacion de datos debe mandarse dicho token por cabecera como x-api-key=token si es incorrecto al que esta en el .env mandara un mensaje que no fue encontrado

las rutas existentes son: 
1) obtener jugadores por nombre http://127.0.0.1:8000/api/v1/players?search=barce&order=asc&page=1  metodo: GET

recibe 3 parámetros todos son opcionales, si no manda search automaticamente retornara la pagina 1 en orden asc

si envia search y el orden y la pagina el backend lo resolvera automaticamente


2) obtener jugadores por equipo http://127.0.0.1:8000/api/v1/team  metodo: POST
recibe un objeto con 2 datos 
{
    "name": "real madrid",
    "page": 1
}

el parametro name sirve para hacer una busqueda por nombre de equipo y devolvera todos los jugadores que pertenezcan a ese equipo y el parametro page para especificar el numero de pagina correspodiente por defecto es 1

3)obtener detalle de 1 jugador http://127.0.0.1:8000/api/v1/players/{id}  metodo: GET

recibe un parametro id que es el id del jugador y mostrara los detalles correspondiente de dicho jugador si no existe dicho jugador enviara un mensaje de que no existe


importante!
las busquedas pueden hacerse tanto minusculas como mayusculas, dará el mismo resultado


