import React from 'react';
import ReactDOM from 'react-dom';

import {
    Route,
    Routes,
    BrowserRouter,
    Navigate
} from 'react-router-dom';

import PlayerPage from '../Pages/PlayerPage';
import PlayerDetail from '../Pages/PlayerDetail';

import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route exact path={"/"} element={<Navigate to="/players" />} />
                    <Route exact path={'/players'} element={<PlayerPage />} />
                    <Route exact path={'/players/details'} element={<PlayerDetail />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
