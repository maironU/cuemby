import React from 'react'

const Arrow = (props) => {

    const {logo, handleArrow, disabled = false} = props
    return (
        <div>
            {!disabled &&
                <img src={logo} className="" alt="" width="60px" style={{cursor: "pointer"}} onClick={handleArrow()}/>
            }
        </div>
    )
}

export default Arrow;

