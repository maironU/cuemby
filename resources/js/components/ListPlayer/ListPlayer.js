import React from 'react'
import ItemPlayer from '../ItemPlayer'
import './style.css'

function generate(data, element) {
    return data.map((value, index) =>
        React.cloneElement(element, {
            key: index,
            player: value
        }),
    );
}

const ListPlayer = ({ players }) => {
    return (
        <>
            <div className="list_players">
                {players.length > 0 ?
                    generate(players, <ItemPlayer />)
                :
                    <div>
                        <span className="text-danger">No hay datos para mostrar</span>
                    </div>
                }
            </div>
        </>
    )
}

export default ListPlayer;

