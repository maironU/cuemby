import React from 'react'
import { Link } from 'react-router-dom'

const ItemPlayer = ({ player, details = true }) => {
    return (
        <div className="col-12 col-xs-12 col-sm-6 col-md-6 col-xl-4 p-2">
            <div className="card">
                <img className="card-img-top" src="https://acf.geeknetic.es/imgri/imagenes/auto/2021/7/14/lna-w00w.jpg?f=webp" alt="Card image cap" />
                <div className="card-body">
                    <h3 className="card-title text-center">{player.name}</h3>
                    <span className="card-text">Nation: { player.nation }</span>
                    <span className="card-text">Position: { player.position }</span>
                    <span className="card-text">Club: { player.club }</span>
                    {details &&
                        <Link to={`/players/details?player_id=${player.id}`}>Detalles</Link>
                    }
                </div>
            </div>
        </div>
    )
}

export default ItemPlayer;

