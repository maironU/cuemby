import React from 'react'

const Search = (props) => {

    let {search, handleSearch} = props

    return (
        <div className="d-flex justify-content-end">
            <input type="text" className="form-control w-25" value={search} style={{marginRight: "20px"}} onChange={handleSearch()} placeholder="Buscar jugadores por equipo"/>
        </div>
    )
}

export default Search;

