import axios from 'axios';
import { HOST } from "./const";

export default class ApiRequest {
    constructor() {
        this.token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
    }

    get(url) {
        return axios.get(`${HOST}${url}`,
            {
                headers: {
                    'x-api-key': `${this.token}`
                },
            });
    }

    post(url, body) {
        return axios.post(`${HOST}${url}`,
            {
                ...body,
             },
            {
                headers: {
                    'x-api-key': `${this.token}`
                },
            });
    }
}
