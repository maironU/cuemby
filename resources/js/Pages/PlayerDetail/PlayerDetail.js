import React, { useState, useEffect } from 'react'
import ItemPlayer from '../../components/ItemPlayer'
import ApiRequest from "../../Utils/request";

const request = new ApiRequest();

const PlayerDetail = () => {

    const [player, setPlayer] = useState(null);

    useEffect(() => {
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        var player_id = urlParams.get('player_id');

        fetchPlayer(player_id)
    }, []);

    const fetchPlayer = (player_id) => {
        request.get(`/players/${player_id}`).then(response => {
            let res = response.data

            if(!res.success) return alert(res.message)
            setPlayer(res.data)
        })
    }

    return (
        <>
            {
                player &&
                    <div className="players">
                        <img src="https://media.vandal.net/m/9-2021/20219261914344_1.jpg" alt="" />
                        <h1 className="text-center">{player.name}</h1>
                        <ItemPlayer player={player} details={false}/>
                    </div>
            }
        </>
    )
}

export default PlayerDetail;

