import React, { useState, useEffect } from 'react'
import './style.css';
import ListPlayer from '../../components/ListPlayer'
import ApiRequest from "../../Utils/request";
import Arrow from '../../components/Arrow'
import Search from '../../components/Search'
import right from '../../../../public/images/arrow-right.png'
import left from '../../../../public/images/arrow-left.png'

const request = new ApiRequest();

const PlayerPage = () => {

    const [dataRes, setDataRes] = useState({
        players : [],
        page: 1,
        totalPages: 1,
        totalItems: 0,
    });
    const [search, setSearch] = useState("");

    useEffect(() => {
        request.get("/players").then(response => {
            let res = response.data
            if(res.error) return alert(res.message)
            setDataRes({
                ...dataRes,
                players: res.players,
                page: res.page,
                totalPages: res.totalPages,
                totalItems: res.totalItems,
            })
        });
    }, []);

    const handleSearch = (e) => {
        let input_search = e.target.value
        setSearch(input_search)
        fetchSearch(input_search, 1);
    }

    const fetchSearch = (input_search, page_new) => {
        let data = { name: input_search, page: page_new }
        request.post("/team", data).then(response => {
            let res = response.data
            setDataRes({
                ...dataRes,
                players: res.players,
                page: res.page,
                totalPages: res.totalPages,
                totalItems: res.totalItems,
            })
        })
    }

    const handleArrowLeft = () => {
        let page_new = dataRes.page -= 1
        fetchSearch(search, page_new)
    }


    const handleArrowRight = () => {
        let page_new = dataRes.page += 1
        fetchSearch(search, page_new)
    }

    return (
        <div className="players">
            <img src="https://media.vandal.net/m/9-2021/20219261914344_1.jpg" alt="" />
            <h1 className="text-center">Players</h1>

            <div className="players-cards">
                <Search search={search} handleSearch={() => handleSearch}/>
                <div className="d-flex flex-column" style={{marginLeft: "10px"}}>
                    <span>Total Páginas: {dataRes.totalPages}</span>
                    <span>Total Jugadores: {dataRes.totalItems}</span>
                </div>

                <ListPlayer players={dataRes.players} />
                <div className="p-3 d-flex justify-content-between w-100">
                    <Arrow logo={left} handleArrow={() => handleArrowLeft} disabled={dataRes.page > 1 ? false : true}/>
                    {dataRes.totalPages != 0 &&
                        <span>Página número: {dataRes.page}</span>
                    }
                    <Arrow logo={right} handleArrow={ () => handleArrowRight} disabled={dataRes.page == dataRes.totalPages || dataRes.totalPages <= 1 ? true : false}/>
                </div>
            </div>
        </div>
    )
}

export default PlayerPage;

